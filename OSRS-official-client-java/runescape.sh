#!/bin/bash
#This script expects you to have java installed
#based on https://github.com/HikariKnight/rsu-client/issues/51
java -cp jagexappletviewer.jar -Dcom.jagex.config=http://oldschool1.runescape.com/jav_config.ws -Xms256m -Xmx256m jagexappletviewer .
#Example of double digit world: java -cp jagexappletviewer.jar -Dcom.jagex.config=http://oldschool33.runescape.com/jav_config.ws -Xms256m -Xmx256m jagexappletviewer .
#you can change the world to whichever world you want
