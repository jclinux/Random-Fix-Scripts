#!/bin/bash
#Credits to /u/galaxeblaffer
#Original post https://www.reddit.com/r/archlinux/comments/47t0vc/nvidia_screen_tearing/d12b68q 
nvidia-settings --assign CurrentMetaMode="$(xrandr | sed -nr '/(\S+) connected (primary )?[0-9]+x[0-9]+(\+\S+).*/{ s//\1: nvidia-auto-select \3 { ForceFullCompositionPipeline = On }, /; H }; ${ g; s/\n//g; s/, $//; p }')"


